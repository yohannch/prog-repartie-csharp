﻿using System;

namespace TP2
{
    class Program
    {
        static void Main(string[] args)
        {
            ///Menu
            int n =6;
            while (n < 0 || n > 5) {
            Console.WriteLine("Tapez le numéro de l'exercice souhaité, ou 0 pour quitter");
            bool err;
            err = int.TryParse(Console.ReadLine(), out n);
            }
            switch(n)
            {
                case 0:
                    break;
                case 1:
                    Console.WriteLine("Exercice 1");
                    for (int i = 1; i <= 9; i++)
                    {
                        Console.WriteLine(i);
                        if (i == 7)
                        {
                            continue;
                        }
                    }
                    for (int i = 7; i <= 9; ++i)
                    {
                        Console.WriteLine(i);
                    }

                    for (int i = 1; i <= 4; i++)
                    {
                        Console.WriteLine(i);
                    }
                    goto case 0;
                case 2:
                    Console.WriteLine("Exercice 2");
                    char a = 'b';
                    String entree;
                    while (a != 'a')
                    {
                        Console.WriteLine("Taper a");
                        entree = Console.ReadLine();
                        entree = entree.Substring(0, 1);
                        bool error = Char.TryParse(entree, out a);
                        Console.WriteLine(a);

                    }
                    goto case 0;
                case 3:
                    Console.WriteLine("Exercice 3");
                    int y = 0;
                    String[] jour = new string[] { "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche" };
                    foreach (String element in jour)
                    {
                        y = y+1;
                        Console.WriteLine(y + " : " + element);

                    }
                    goto case 0;
                case 4:
                    Console.WriteLine("Exercice 4");
                    int val=0;
                    int somme = 0;
                    String valentree;
                    while (somme < 50) {
                        
                            Console.WriteLine("Taper  un nombre");
                            valentree = Console.ReadLine();                    
                            bool erreur = int.TryParse(valentree, out val);
                            Console.WriteLine(val);
                            somme = somme + val;

                    }
                    Console.WriteLine("La somme est de : " + somme);
                    
                    goto case 0;
                case 5:
                    Console.WriteLine("Exercice 5");
                    goto case 0;
                default:
                    Console.WriteLine("Saisie invalide");
                    Console.ReadKey();
                    goto case 0;
            }



            
            



            Console.ReadKey();
        }

    }
}
